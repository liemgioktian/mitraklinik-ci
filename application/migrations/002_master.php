<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_master extends CI_Migration {

  function up () {

    $this->db->query("
      CREATE TABLE `menu` (
        `uuid` varchar(255) NOT NULL,
        `name` varchar(255) NOT NULL,
        `icon` varchar(255) NOT NULL,
        `url` varchar(255) NOT NULL,
        `roles` varchar(255) NOT NULL,
        PRIMARY KEY (`uuid`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8
    ");

    $this->db->query("
      CREATE TABLE `staff` (
        `uuid` varchar(255) NOT NULL,
        `nip` varchar(255) NOT NULL,
        `fid` varchar(255) NOT NULL,
        `name` varchar(255) NOT NULL,
        `dob` date NOT NULL,
        `gender` char(1) NOT NULL,
        `npwp` varchar(255) NOT NULL,
        `address` varchar(255) NOT NULL,
        `phone` varchar(255) NOT NULL,
        `email` varchar(255) NOT NULL,
        `start_contract` date NOT NULL,
        `end_contract` date NOT NULL,
        `employment_status` varchar(255) NOT NULL,
        `role` varchar(255) NOT NULL,
        `bank` varchar(255) NOT NULL,
        `bank_account_number` varchar(255) NOT NULL,
        `bloodgroup` varchar(255) NOT NULL,
        `registration_number` varchar(255) NOT NULL,
        `license_number` varchar(255) NOT NULL,
        `status` varchar(255) NOT NULL,
        PRIMARY KEY (`uuid`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8
    ");

    $this->db->query("
      CREATE TABLE `patient` (
        `uuid` varchar(255) NOT NULL,
        `name` varchar(255) NOT NULL,
        `family_card_number` varchar(255) NOT NULL,
        `identity_number` varchar(255) NOT NULL,
        `gender` char(1) NOT NULL,
        `bloodgroup` varchar(255) NOT NULL,
        `dob` date NOT NULL,
        `phone` varchar(255) NOT NULL,
        `email` varchar(255) NOT NULL,
        `address` varchar(255) NOT NULL,
        `company` varchar(255) NOT NULL,
        `nip` varchar(255) NOT NULL,
        `status` varchar(255) NOT NULL,
        `insurance` varchar(255) NOT NULL,
        `description` varchar(255) NOT NULL,
        PRIMARY KEY (`uuid`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8
    ");

    $this->db->query("
      CREATE TABLE `mkdepartment` (
        `uuid` varchar(255) NOT NULL,
        `name` varchar(255) NOT NULL,
        PRIMARY KEY (`uuid`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8
    ");

    $this->db->query("
      CREATE TABLE `mkdoctor` (
        `uuid` varchar(255) NOT NULL,
        `staff` varchar(255) NOT NULL,
        `department` varchar(255) NOT NULL,
        `tariff` int(11) NOT NULL,
        `str_number` varchar(255) NOT NULL,
        `sip_number` varchar(255) NOT NULL,
        PRIMARY KEY (`uuid`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8
    ");

    $this->db->query("
      CREATE TABLE `company_type` (
        `uuid` varchar(255) NOT NULL,
        `name` varchar(255) NOT NULL,
        PRIMARY KEY (`uuid`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8
    ");

    $this->db->query("
      CREATE TABLE `company` (
        `uuid` varchar(255) NOT NULL,
        `code` varchar(255) NOT NULL,
        `name` varchar(255) NOT NULL,
        `type` varchar(255) NOT NULL,
        `address` varchar(255) NOT NULL,
        `contract_number` varchar(255) NOT NULL,
        `start_contract` varchar(255) NOT NULL,
        `end_contract` varchar(255) NOT NULL,
        `contact_person` varchar(255) NOT NULL,
        `phone_number` varchar(255) NOT NULL,
        `status` varchar(255) NOT NULL,
        PRIMARY KEY (`uuid`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8
    ");

    $this->db->query("
      CREATE TABLE `insurance` (
        `uuid` varchar(255) NOT NULL,
        `company` varchar(255) NOT NULL,
        `capitation_amount` int(11) NOT NULL,
        `capitation_payment_date` date NOT NULL,
        PRIMARY KEY (`uuid`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8
    ");

    $this->db->query("
      CREATE TABLE `supplier` (
        `uuid` varchar(255) NOT NULL,
        `company` varchar(255) NOT NULL,
        `lead_time` int(11) NOT NULL,
        PRIMARY KEY (`uuid`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8
    ");

    $this->db->query("
      CREATE TABLE `contact` (
        `uuid` varchar(255) NOT NULL,
        `code` varchar(255) NOT NULL,
        `name` varchar(255) NOT NULL,
        `description` varchar(255) NOT NULL,
        `contact` varchar(255) NOT NULL,
        `status` varchar(255) NOT NULL,
        PRIMARY KEY (`uuid`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8
    ");

    $this->db->query("
      CREATE TABLE `bank` (
        `uuid` varchar(255) NOT NULL,
        `code` varchar(255) NOT NULL,
        `name` varchar(255) NOT NULL,
        `account_number` varchar(255) NOT NULL,
        `description` varchar(255) NOT NULL,
        `contact` varchar(255) NOT NULL,
        `status` varchar(255) NOT NULL,
        `estimates` varchar(255) NOT NULL,
        PRIMARY KEY (`uuid`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8
    ");

    $this->db->query("
      CREATE TABLE `group_of_goods` (
        `uuid` varchar(255) NOT NULL,
        `code` varchar(255) NOT NULL,
        `name` varchar(255) NOT NULL,
        `group` varchar(255) NOT NULL,
        PRIMARY KEY (`uuid`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8
    ");

    $this->db->query("
      CREATE TABLE `mkmedicine_category` (
        `uuid` varchar(255) NOT NULL,
        `name` varchar(255) NOT NULL,
        `description` varchar(255) NOT NULL,
        PRIMARY KEY (`uuid`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8
    ");

    $this->db->query("
      CREATE TABLE `location` (
        `uuid` varchar(255) NOT NULL,
        `code` varchar(255) NOT NULL,
        `name` varchar(255) NOT NULL,
        `in_charge` varchar(255) NOT NULL,
        PRIMARY KEY (`uuid`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8
    ");

    $this->db->query("
      CREATE TABLE `unit_of_goods` (
        `uuid` varchar(255) NOT NULL,
        `code` varchar(255) NOT NULL,
        `unit` varchar(255) NOT NULL,
        `abbreviation` varchar(255) NOT NULL,
        `contains` varchar(255) NOT NULL,
        PRIMARY KEY (`uuid`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8
    ");

    $this->db->query("
      CREATE TABLE `mkmedicine` (
        `uuid` varchar(255) NOT NULL,
        `code` varchar(255) NOT NULL,
        `barcode` varchar(255) NOT NULL,
        `name` varchar(255) NOT NULL,
        `manufacture` varchar(255) NOT NULL,
        `ingredients` varchar(255) NOT NULL,
        `unit` varchar(255) NOT NULL,
        `category` varchar(255) NOT NULL,
        `group` varchar(255) NOT NULL,
        `tax` int(11) NOT NULL,
        `margin` int(11) NOT NULL,
        `min_stock` int(11) NOT NULL,
        `max_stock` int(11) NOT NULL,
        `initial_daily_needs` int(11) NOT NULL,
        `suppliers` varchar(255) NOT NULL,
        PRIMARY KEY (`uuid`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8
    ");

    $this->db->query("
      CREATE TABLE `medicine_stock` (
        `uuid` varchar(255) NOT NULL,
        `medicine` varchar(255) NOT NULL,
        `supplier` varchar(255) NOT NULL,
        `stock` varchar(255) NOT NULL,
        `purchase_price` int(11) NOT NULL,
        `sales_price` varchar(255) NOT NULL,
        `location` varchar(255) NOT NULL,
        PRIMARY KEY (`uuid`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8
    ");

    $this->db->query("
      CREATE TABLE `medical_tool` (
        `uuid` varchar(255) NOT NULL,
        `code` varchar(255) NOT NULL,
        `barcode` varchar(255) NOT NULL,
        `name` varchar(255) NOT NULL,
        `manufacture` varchar(255) NOT NULL,
        `unit` varchar(255) NOT NULL,
        `group` varchar(255) NOT NULL,
        `tax` int(11) NOT NULL,
        `margin` int(11) NOT NULL,
        `min_stock` int(11) NOT NULL,
        `max_stock` int(11) NOT NULL,
        `initial_daily_needs` int(11) NOT NULL,
        `suppliers` varchar(255) NOT NULL,
        PRIMARY KEY (`uuid`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8
    ");

    $this->db->query("
      CREATE TABLE `medical_tool_stock` (
        `uuid` varchar(255) NOT NULL,
        `medical_tool` varchar(255) NOT NULL,
        `supplier` varchar(255) NOT NULL,
        `stock` varchar(255) NOT NULL,
        `purchase_price` int(11) NOT NULL,
        `sales_price` varchar(255) NOT NULL,
        `location` varchar(255) NOT NULL,
        PRIMARY KEY (`uuid`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8
    ");

    $this->db->query("
      CREATE TABLE `diagnose` (
        `uuid` varchar(255) NOT NULL,
        `code` varchar(255) NOT NULL,
        `name` varchar(255) NOT NULL,
        `nama` varchar(255) NOT NULL,
        `description` varchar(255) NOT NULL,
        PRIMARY KEY (`uuid`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8
    ");

    $this->db->query("
      CREATE TABLE `asset` (
        `uuid` varchar(255) NOT NULL,
        `name` varchar(255) NOT NULL,
        `group` varchar(255) NOT NULL,
        `purchase_price` int(11) NOT NULL,
        `depreciation_per_month` int(11) NOT NULL,
        `depreciation_date` date NOT NULL,
        `status` varchar(255) NOT NULL,
        PRIMARY KEY (`uuid`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8
    ");

    $this->db->query("
      CREATE TABLE `mkservice` (
        `uuid` varchar(255) NOT NULL,
        `code` varchar(255) NOT NULL,
        `name` varchar(255) NOT NULL,
        `department` varchar(255) NOT NULL,
        PRIMARY KEY (`uuid`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8
    ");

    $this->db->query("
      CREATE TABLE `service_requirement` (
        `uuid` varchar(255) NOT NULL,
        `service` varchar(255) NOT NULL,
        `type` varchar(255) NOT NULL,
        `item` varchar(255) NOT NULL,
        `qty` varchar(255) NOT NULL,
        `price` int(11) NOT NULL,
        PRIMARY KEY (`uuid`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8
    ");

    // $this->db->query("
    //   CREATE TABLE `staff` (
    //     `uuid` varchar(255) NOT NULL,
    //     PRIMARY KEY (`uuid`)
    //   ) ENGINE=InnoDB DEFAULT CHARSET=utf8
    // ");

  }

  function down () {
    foreach (array(
      'menu',
      'staff',
      'mkdepartment',
      'mkdoctor',
      'company_type',
      'company',
      'insurance',
      'contact',
      'bank',
      'group_of_goods',
      'mkmedicine_category',
      'location',
      'unit_of_goods',
      'mkmedicine',
      'medicine_stock',
      'medical_tool',
      'medical_tool_stock',
      'diagnose',
      'asset',
      'mkservice',
      'service_requirement',
      'supplier',
      'patient'
    ) as $table) $this->db->query("DROP TABLE IF EXISTS `$table`");
  }

}
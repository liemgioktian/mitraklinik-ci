<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_config extends CI_Migration {

  function up () {
    // $templine = '';
    // $lines = file('./uploads/install.sql');
    // foreach ($lines as $line) {
    //   if (substr($line, 0, 2) == '--' || $line == '') continue;
    //   $templine .= $line;
    //   if (substr(trim($line), -1, 1) == ';') {
    //     $this->db->query($templine);
    //     $templine = '';
    //   }
    // }

    $this->db->query("
      INSERT INTO `accountant` (`accountant_id`, `name`, `email`, `password`, `address`, `phone`)
      VALUES
        ('','Kasir','kasir@doktermoez.com','7110eda4d09e062aa5e4a390b0a572ac0d2c0220',NULL,NULL)
    ");

    $this->db->query("
      INSERT INTO `admin` (`admin_id`, `name`, `email`, `password`)
      VALUES
        (123,'doktermoez','doktermoez@gmail.com','90139c9f6e31450fb011c15648c6dc844f8223fd'),
        (234,'admin','doktermoez@klinikita.co.id','7b902e6ff1db9f560443f2048974fd7d386975b0')
    ");

    $this->db->query("
      INSERT INTO `department` (`department_id`, `name`, `description`, `facilities`)
      VALUES
        (3,'Dokter Umum','Praktik Dokter Umum',NULL),
        (2,'Dokter Gigi','Praktik Dokter Gigi',NULL),
        (4,'Spesialis Kandungan','Dokter Spesialis Kandungan',NULL),
        (5,'Spesialis lainya','Daftar bagian dapat ditambah sesuai kebutuhan',NULL)
    ");

    $this->db->query("
      INSERT INTO `doctor` (`doctor_id`, `name`, `email`, `password`, `address`, `phone`, `department_id`, `profile`, `social_links`)
      VALUES
        (104,'Drg. Demo Demo','doktergigi@doktermoez.com','7110eda4d09e062aa5e4a390b0a572ac0d2c0220','Jl. Demo Sistem ','+628112933000',2,'Profil dokter gigi ini dapat ditulis disini dengan panjang beberapa karakter. dan dapat disematkan video atau gambar untuk memberikan efek yang baik <br>','[{\"facebook\":\"facebook.dom\\/dokter\",\"twitter\":\"twitter.com\\/dokter\",\"google_plus\":\"google.com\\/dokter\",\"linkedin\":\"lingkedin.com\\/dokter\"}]'),
        (103,'dr. Demo Domo','dokter@doktermoez.com','7110eda4d09e062aa5e4a390b0a572ac0d2c0220','Jl. Demo Domo Sistem','+628112933000',3,'Dokter ini mempunyai profil yang dapat dituliskan disini dengan beberapa karakter didalamnya<br>','[{\"facebook\":\"facebook.dom\\/dokter\",\"twitter\":\"twitter.com\\/dokter\",\"google_plus\":\"google.com\\/dokter\",\"linkedin\":\"lingkedin.com\\/dokter\"}]'),
        (105,'dr. Demo Domo Sp.OG','dokterspesialis@doktermoez.com','7110eda4d09e062aa5e4a390b0a572ac0d2c0220','Jl. Demo Domo','+628112933000',4,'Profil dokter ini dapat ditulis disini dengan dapat disematkan foto atau video dalam profil ini<br>','[{\"facebook\":\"facebook.dom\\/dokter\",\"twitter\":\"twitter.com\\/dokter\",\"google_plus\":\"google.com\\/dokter\",\"linkedin\":\"lingkedin.com\\/dokter\"}]'),
        (106,'dr. Deo Domo Sp.Lain','dokterlain@doktermoez.com','7110eda4d09e062aa5e4a390b0a572ac0d2c0220','Jl. Demo Domo','+628112933000',5,'Profil dokter ini dapat ditulis disini dan profil ini dapat disematkan gambar atau video tentang dokter ini<br>','[{\"facebook\":\"facebook.dom\\/dokter\",\"twitter\":\"twitter.com\\/dokter\",\"google_plus\":\"google.com\\/dokter\",\"linkedin\":\"lingkedin.com\\/dokter\"}]');
    ");

    $this->db->query("
      INSERT INTO `facility` (`facility_id`, `department_id`, `title`, `description`)
      VALUES
        (1,1,'Medical Check Up','Pemeriksaan MCU oleh dokter umum')
    ");

    $this->db->query("
      INSERT INTO `frontend_blog` (`frontend_blog_id`, `title`, `short_description`, `blog_post`, `posted_by`, `timestamp`, `published`)
      VALUES
        (1,'Bekerja di rumah','Jika anda sakit anda harus tetapbekerja','Tes buat blog<br>','admin-123',1517702400,1)
    ");

    $this->db->query("TRUNCATE `frontend_settings`");
    $this->db->query("
      INSERT INTO `frontend_settings` (`frontend_settings_id`, `type`, `description`) VALUES
      (1, 'theme', 'default'),
      (2, 'hospital_name', 'Domo medical System'),
      (3, 'emergency_contact', '08112922911'),
      (4, 'opening_hours', '[{\"monday_friday\":\"24 jam \",\"saturday\":\"24 jam\",\"sunday\":\"24 jam\"}]'),
      (5, 'social_links', '[{\"facebook\":\"http:\\/\\/facebook.com\\/doktermoez\",\"twitter\":\"https:\\/\\/twitter.com\\/doktermoez\",\"google_plus\":\"https:\\/\\/google.com\",\"youtube\":\"https:\\/\\/youtube.com\\/doktermoez\"}]'),
      (6, 'email', 'info@mitramedika.com'),
      (7, 'copyright_text', '(C) 2018 oleh Dokter Moez Indonesia'),
      (8, 'slider', '[{\"title\":\"Kami ingin berbagi bersama \",\"description\":\"Pasien, dokter, perawat dan petugas lainnya dapat menggunakan sistem ini untuk mendapatkan informasi tentang klinik dan pasien yang terdafar dalam sistem ini\",\"image\":\"doctor-1807475_1280.png\"},{\"title\":\"Kami ingin mengerti anda\",\"description\":\"Kami menyadari bahwa bekerja di klinik kesehatan membutuhkan kerja tim, kami ingin berbagi informasi dan kemudahan melalui teknologi informasi berbasiskan web\",\"image\":\"nurse-2141808_1920.jpg\"},{\"title\":\"Menjadi lebih baik\",\"description\":\"Dokter, Pasien, Perawat, Apoteker, Kasir, Analis Laborat dan Petugas Pendaftaran akan mendapatkan identitas sendiri-sendiri untuk memudahkan akses dari mana saja\",\"image\":\"smartphone-1894723_1920.jpg\"}]'),
      (9, 'homepage_welcome_section', '[{\"title\":\"Selamat Datang\",\"description\":\"Selamat datang di halaman contoh Sistem Informasi Klinik yang dipersembahkan oleh PT. Dokter Moez Indonesia.\\r\\n\\r\\nHalaman contoh ini akan memberikan informasi kepada anda tentang bagian-bagian dari halaman website sebuah klinik. Website ini terintegrasi bagi para pengunjung dengan para petugas yang ada di dalam klinik termasuk mitra dokter\",\"image\":\"screen-1839500_1280.jpg\"}]'),
      (10, 'service_section', '[{\"title\":\"Layanan Kami\",\"description\":\"Berbagi bersama anda adalah hal yang terbaik untuk memberikan manfaat sesama\"}]'),
      (11, 'about_us', 'Dokter Moez Medical System atau disingkat Domo Medical System adalah bagian dari usaha PT. Dokter Moez Indonesia yang terletak di Jl. Abdulrahman Saleh Kav 783 Semarang. Kami mempunyai merk Klinik layanan kesehatan yakni Klinikita Indonesia, yang sekarang telah diwaralabakan sejak tahun 2014. <br><br>\r\n\r\n<iframe width=\"740\" height=\"416\" src=\"https://www.youtube.com/embed/jNR9M-qJ3cU\" frameborder=\"0\" allow=\"autoplay; encrypted-media\" allowfullscreen></iframe>\r\n\r\n<<br><br><br> \r\nSilahkan kunjungi kantor kami di sini\r\n<<br><br><br> \r\n<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3960.0264296834466!2d110.37709731354158!3d-7.006170670567468!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e708bf8c5ce1d6f%3A0x1cf3c8297fa9bf72!2sPT+Dokter+Moez+Indonesia!5e0!3m2!1sid!2sid!4v1517785533008\" width=\"740\" height=\"600\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>\r\n\r\n\r\n'),
      (12, 'recaptcha', '[{\"site_key\":\"6LdgM0QUAAAAACLMhAZ93xXnHpO76GOxZi02HQsp\",\"secret_key\":\"6LdgM0QUAAAAAINEsJk8_zQQIQeL5S2vUkqmu8WE\"}]')
    ");

    $this->db->query("
      INSERT INTO `laboratorist` (`laboratorist_id`, `name`, `email`, `password`, `address`, `phone`)
      VALUES
        (2,'Analis','analis@doktermoez.com','7110eda4d09e062aa5e4a390b0a572ac0d2c0220',NULL,NULL);
    ");

    $this->db->query("
      INSERT INTO `medicine_category` (`medicine_category_id`, `name`, `description`)
      VALUES
        (1,'Generik','Obat Generik')
    ");

    $this->db->query("
      INSERT INTO `nurse` (`nurse_id`, `name`, `email`, `password`, `address`, `phone`)
      VALUES
        (2,'Perawat','perawat@doktermoez.com','7110eda4d09e062aa5e4a390b0a572ac0d2c0220',NULL,NULL)
    ");
    $this->db->query("
      INSERT INTO `patient` (`patient_id`, `code`, `name`, `email`, `password`, `address`, `phone`, `sex`, `birth_date`, `age`, `blood_group`, `account_opening_timestamp`)
      VALUES
        (1,'0d4d45a','Pasien','pasien@doktermoez.com','7110eda4d09e062aa5e4a390b0a572ac0d2c0220',NULL,NULL,NULL,NULL,NULL,NULL,NULL)
    ");

    $this->db->query("
      INSERT INTO `pharmacist` (`pharmacist_id`, `name`, `email`, `password`, `address`, `phone`)
      VALUES
        (2,'Apoteker','apoteker@doktermoez.com','7110eda4d09e062aa5e4a390b0a572ac0d2c0220',NULL,NULL)
    ");

    $this->db->query("
      INSERT INTO `receptionist` (`receptionist_id`, `name`, `email`, `password`, `address`, `phone`)
      VALUES
        (3,'Pendaftaran','pendaftaran@doktermoez.com','7110eda4d09e062aa5e4a390b0a572ac0d2c0220',NULL,NULL)
    ");

    $this->db->query("
      INSERT INTO `service` (`service_id`, `title`, `description`)
      VALUES
        (1,'Dokter Umum','Layanan praktik dokter umum oleh dokter umum yang berpengalaman'),
        (2,'Dokter Gigi ','Layanan praktik dokter Gigi'),
        (3,'Spesialis Kandungan','Praktik dokter spesialis kandungan'),
        (4,'Spesialis lainnya','Kami akan menyediakan layanan praktik dokter spesialis lainnya.')
    ");

    $this->db->query("TRUNCATE `settings`");
    $this->db->query("
      INSERT INTO `settings` (`settings_id`, `type`, `description`) VALUES
      (1, 'system_name', 'Domo Medical System'),
      (2, 'system_title', 'Domo Medical System'),
      (3, 'address', NULL),
      (4, 'phone', '+628112933000'),
      (5, 'paypal_email', 'doktermoez@gmail.com'),
      (6, 'currency', 'Rp'),
      (7, 'system_email', 'doktermoez@gmail.com'),
      (9, 'purchase_code', '1234567890'),
      (11, 'language', 'Indonesia'),
      (12, 'text_align', 'left-to-right'),
      (13, 'system_currency_id', '1'),
      (14, 'clickatell_user', 'doktermoez@gmail.com'),
      (15, 'clickatell_password', 'Abc7624455'),
      (16, 'clickatell_api_id', 'n9acrCCVQICgri4OpK2WuQ== '),
      (17, 'version', '4.1')
    ");
  }

  function down () {
    $tables = array();
    foreach ($this->db->query('SHOW TABLES')->result() as $table) foreach ($table as $t) $tables[] = $t;
    foreach ($tables as $table) if ($table !== 'migrations') $this->db->query("DROP TABLE IF EXISTS $table");
  }

}
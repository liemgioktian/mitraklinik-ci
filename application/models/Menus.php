<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Menus extends MY_Model {

  function __construct () {
    parent::__construct();
    $this->form  = array ();
    $this->form[]= array(
      'name'    => 'parent',
      'label'   => 'Parent',
      'options' => array(array('text' => '', 'value' => '')),
      'attributes' => array(
        array('data-autocomplete' => 'true'), 
        array('data-model' => 'Menus'), 
        array('data-field' => 'name')
      ),
    );
    $this->form[]= array(
      'name'    => 'name',
      'label'   => 'Name',
    );
    $this->form[]= array(
      'name'    => 'icon',
      'label'   => 'Icon',
    );
    $this->form[]= array(
      'name'    => 'url',
      'label'   => 'URL',
    );
    $options = array();
    foreach (glob(APPPATH . 'views/backend' . '/*' , GLOB_ONLYDIR) as $dir) {
      $addr = explode('/', $dir);
      $role = end($addr);
      $options[] = array('text' => ucfirst($role), 'value' => $role);
    }
    $this->form[]= array(
      'name'    => 'roles',
      'label'   => 'Roles',
      'multiple'=> true,
      'options' => $options
    );
  }

}
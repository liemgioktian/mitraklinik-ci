<div class="sidebar-menu">

    <header class="logo-env" >
        <!-- logo -->
        <div class="logo" style="">
            <a href="<?php echo base_url(); ?>">
                <img src="uploads/logo.png"  style="max-height:60px;"/>
            </a>
        </div>

        <!-- logo collapse icon -->
        <div class="sidebar-collapse" style="">
            <a href="#" class="sidebar-collapse-icon with-animation">
                <i class="entypo-menu"></i>
            </a>
        </div>

        <!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
        <div class="sidebar-mobile-menu visible-xs">
            <a href="#" class="with-animation">
                <i class="entypo-menu"></i>
            </a>
        </div>
    </header>

    <div class="sidebar-user-info">
        <div class="sui-normal">
            <a href="#" class="user-link">
              <img src="<?= $photo_profile ?>" alt="" class="img-circle" style="height:44px;">

              <span><?php echo get_phrase('welcome'); ?>,</span>
              <strong><?php
                echo $this->db->get_where($this->session->userdata('login_type'), array($this->session->userdata('login_type') . '_id' =>
                    $this->session->userdata('login_user_id')))->row()->name;
                ?>
              </strong>
            </a>
        </div>
        <div class="sui-hover inline-links animate-in"><!-- You can remove "inline-links" class to make links appear vertically, class "animate-in" will make A elements animateable when click on user profile -->

            <a href="<?php echo base_url(); ?>index.php?<?php echo $account_type; ?>/manage_profile">
                <i class="entypo-pencil"></i>
                <?php echo get_phrase('edit_profile'); ?>
            </a>
            <a href="<?php echo base_url(); ?>index.php?<?php echo $account_type; ?>/manage_profile">
                <i class="entypo-lock"></i>
                <?php echo get_phrase('change_password'); ?>
            </a>

            <span class="close-sui-popup">×</span><!-- this is mandatory -->
        </div>
    </div>

    <ul id="main-menu" class="">
        <!-- add class "multiple-expanded" to allow multiple submenus to open -->
        <!-- class "auto-inherit-active-class" will automatically add "active" class for parent elements who are marked already with class "active" -->

      <?php if ('admin' === $account_type): ?>
      <li>
        <a href="<?= site_url('Menu') ?>" class="menu">
          <i class="fa fa-bars"></i>
          <span>Menu System</span>
        </a>
      </li>
      <?php endif ?>

      <?php foreach ($menu as $m): ?>
        <?php if (empty ($m['submenu'])): ?>
          <li>
            <a href="<?= site_url($m['menu']->url) ?>" class="menu">
              <i class="fa fa-<?= $m['menu']->icon ?>"></i>
              <span><?= $m['menu']->name ?></span>
            </a>
          </li>
        <?php else: ?>
          <li class="root-level has-sub">
            <a href="#">
              <i class="fa fa-<?= $m['menu']->icon ?>"></i>
              <span><?= $m['menu']->name ?></span>
            </a>
            <ul>
              <?php foreach ($m['submenu'] as $sub): ?>
                <li>
                  <a href="<?= site_url($sub->url) ?>" class="menu submenu">
                    <i class="fa fa-<?= $sub->icon ?>"></i>
                    <span><?= $sub->name ?></span>
                  </a>
                </li>
              <?php endforeach ?>
            </ul>
          </li>
        <?php endif ?>
    <?php endforeach ?>

    </ul>

</div>

<script type="text/javascript">
  var activemenu = $('a[href="' + window.location.href + '"].menu')
  activemenu.find('span').css('color', 'white')
  if (activemenu.is('.submenu')) activemenu.parent().parent().parent('li').addClass('opened')
</script>
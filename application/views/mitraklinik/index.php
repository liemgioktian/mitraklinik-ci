<!DOCTYPE html>
<html lang="en" dir="<?= 'right-to-left' === $text_align ? 'rtl' : '' ; ?>">
  <head>

    <title><?= $page_title; ?> - <?= $system_title; ?></title>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="<?= $system_title ?>" />
    <meta name="author" content="<?= $system_title ?>" />

    <?php include APPPATH . 'views/mitraklinik/includes_top.php'; ?>

  </head>
  <body class="page-body">
    <div class="page-container <?php if ('right-to-left' === $text_align) echo 'right-sidebar'; if ($page_name == 'frontend') echo 'sidebar-collapsed'?>">

      <?php include APPPATH . 'views/mitraklinik/navigation.php'; ?>

      <div class="main-content">

        <?php include APPPATH . 'views/mitraklinik/header.php'; ?>

        <h3 style="margin:20px 0px; color:#818da1; font-weight:200;">
          <i class="entypo-right-circled"></i>
          <?php echo $page_title; ?>
        </h3>

        <?php include APPPATH .'views/mitraklinik/'. $page_name . '.php'; ?>
        <?php include APPPATH .'views/mitraklinik/footer.php'; ?>

      </div>
    </div>

    <?php include APPPATH .'views/mitraklinik/modal.php'; ?>
    <?php include APPPATH .'views/mitraklinik/includes_bottom.php'; ?>

  </body>
</html>
<div class="row">
  <div class="col-md-12">
    <div class="panel panel-primary" data-collapsed="0">

      <div class="panel-body">
        <?php echo form_open(site_url($current['controller']), array('class' => 'form-horizontal form-groups', 'enctype' => 'multipart/form-data')); ?>
        <input type="hidden" name="last_submit" value="<?= time() ?>">        

        <?php $uuid = 0; foreach ($form as $field) :  if ('uuid' === $field['name']) $uuid = $field['value'] ?>
        <div class="form-group">
          <label class="col-sm-3 control-label"><?= 'hidden' === $field['type'] ? '' : $field['label'] ?></label>
          <div class="col-sm-7">
            <?php if(in_array($field['type'], array('text', 'hidden'))): ?>
              <input class="form-control" type="<?= $field['type'] ?>" value="<?= $field['value'] ?>" name="<?= $field['name'] ?>" <?= $field['attr'] ?>>
            <?php elseif('select' === $field['type']): ?>
              <select class="form-control" name="<?= $field['name'] ?>" <?= $field['attr'] ?>>
                <?php foreach ($field['options'] as $opt): ?>
                <option <?= $opt['value'] === $field['value'] || (is_array($field['value']) && in_array($opt['value'], $field['value'])) ? 'selected="selected"':'' ?> value="<?= $opt['value'] ?>"><?= $opt['text'] ?></option>
                <?php endforeach ?>
              </select>
            <?php endif ?>
          </div>
        </div>
        <?php endforeach ?>

        <div class="form-group">
          <div class="col-sm-7 col-sm-offset-3">
            <button class="btn btn-primary"><i class="fa fa-save"></i> &nbsp; Save</button>
            <?php if (0 !== $uuid): ?>
            <a href="<?= site_url($current['controller'] . "/delete/$uuid") ?>" class="btn btn-danger"><i class="fa fa-trash-o"></i> &nbsp; Delete</a>
            <?php endif ?>
            <a href="<?= site_url($current['controller']) ?>" class="btn btn-info"><i class="fa fa-arrow-left"></i> &nbsp; Cancel</a>
          </div>
        </div>

        <?php echo form_close(); ?>
      </div>

    </div>
  </div>
</div>
<script type="text/javascript">
  window.onload = function () {
    $('select').select2()
    $('select[data-autocomplete]').each(function () {
      var model = $(this).attr('data-model')
      var field = $(this).attr('data-field')
      $(this).select2('destroy').select2({
        ajax: {
          url: '<?= site_url($current['controller']) ?>/select2/' + model + '/' + field,
          type: 'POST', dataType: 'json'
        }
      })
    })
    $('[data-date="datepicker"]').datepicker({format: 'yyyy-mm-dd', autoHide: true})
  }
</script>
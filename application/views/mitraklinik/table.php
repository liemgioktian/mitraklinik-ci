<div class="text-right">
  <a href="<?= site_url($current['controller'] . '/create') ?>" class="btn btn-primary">
    <i class="fa fa-plus"></i>&nbsp;Add New <?= $current['controller'] ?>
  </a>
</div>
<table class="table table-bordered table-striped datatable table-model">
</table>
<script type="text/javascript">
  window.onload = function () {
    $('.table-model').DataTable({
      bProcessing: true,
      aaData: <?= json_encode($records) ?>,
      aoColumns: <?= json_encode($thead) ?>,
      fnRowCallback: function(nRow, aData, iDisplayIndex ) {
        $(nRow).css('cursor', 'pointer').click( function () {
          window.location.href = '<?= site_url($current['controller']) ?>/read/' + aData.uuid
        })
      }
    })
  }
</script>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Model extends CI_Model {

  var $table, $thead;

  function __construct () {
    parent::__construct();
    $this->load->database();
    $this->table = strtolower($this->router->fetch_class());
    $this->thead = array(
      (object) array('mData' => 'name', 'sTitle' => 'Name'),
    );
  }

  function save ($record) {
    foreach ($record as $field => &$value) if (is_array($value)) $value = implode(',', $value);
    return isset ($record['uuid']) ? $this->update($record) : $this->create($record);
  }

  function create ($record) {
    $generate = $this->db->select('UUID() uuid', false)->get()->row_array();
    $uuid = $generate['uuid'];
    $record['uuid'] = $uuid;
    $this->db->insert($this->table, $record);
    return $uuid;
  }

  function update ($record) {
    $this->db->where('uuid', $record['uuid'])->update($this->table, $record);
    return $record['uuid'];
  }

  function findOne ($param) {
    if (!is_array($param)) $param = array('uuid' => $param);
    return $this->db->get_where($this->table, $param)->row_array();
  }

  function find ($param = array()) {
    return $this->db->get_where($this->table, $param)->result();
  }

  function findIn ($field, $value) {
    return $this->db->where_in($field, $value)->get($this->table)->result();
  }

  function select2 ($field, $term) {
    return $this->db
      ->select("uuid as id", false)
      ->select("$field as text", false)
      ->like($field, $term)->get($this->table)->result();
  }

  function delete ($uuid) {
    return $this->db->where('uuid', $uuid)->delete($this->table);
  }

  function getForm ($uuid = false) {
    $form = $uuid ? $this->prepopulate($uuid) : $this->form;

    if ($uuid) $form[] = array(
      'name' => 'uuid',
      'type' => 'hidden',
      'value'=> $uuid,
      'label'=> 'UUID'
    );

    foreach ($form as &$f) {
      if (!isset ($f['attributes'])) $f['attributes']   = array();
      if (isset ($f['options'])) $f['type'] = 'select';
      if (isset ($f['multiple'])) {
        $f['name']= $f['name'] . '[]';
        $f['attributes'][] = array('multiple' => 'true');
      }
      if (!isset ($f['type'])) $f['type']   = 'text';
      if (!isset ($f['value'])) $f['value']       = '';
      if (!isset ($f['required'])) $f['required'] = '';
      else $f['required'] = 'required="required"';

      $f['disabled'] = !isset($f['disabled']) ? '' : 'disabled="disabled"';

      if (isset($f['suggestion'])) {
        $f['attributes'][] = 'suggestion';
        $fname = str_replace('[]', '', $f['name']);
        if (isset ($f['multiple'])) {
          $alloptions = array();
          $f['options'] = array();
          foreach ($this->db->select($fname)->get($this->table)->result() as $record)
            if (strlen($record->$fname) > 0) foreach (explode(',', $record->$fname) as $option) $alloptions[] = $option;
          foreach (array_unique ($alloptions) as $distinct) $f['options'][] = array('text' => $distinct, 'value' => $distinct);
        } else {
          $f['options'] = array();
          foreach ($this->db->select($fname)->distinct()->get($this->table)->result() as $record)
            if (strlen($record->$fname) > 0) $f['options'][] = array('text' => $record->$fname, 'value' => $record->$fname);
        }
      }

      $f['attr'] = '';
      foreach ($f['attributes'] as $attribute) foreach ($attribute as $key => $value) $f['attr'] .= " $key=\"$value\"";
    }
    return $form;
  }

  function prepopulate ($uuid) {
    $record = $this->findOne($uuid);
    foreach ($this->form as &$f) {
      if (isset ($f['attributes']) && in_array(array('data-autocomplete' => 'true'), $f['attributes'])) {
        $model = '';
        $field = '';
        foreach ($f['attributes'] as $attr) foreach ($attr as $key => $value) switch ($key) {
          case 'data-model': $model = $value; break;
          case 'data-field': $field = $value; break;
        }
        $this->load->model($model);
        foreach ($this->$model->findIn('uuid', explode(',', $record[$f['name']])) as $option)
          $f['options'][] = array('text' => $option->$field, 'value' => $option->uuid);
      }
      if (isset ($f['multiple'])) $f['value'] = explode(',', $record[$f['name']]);
      else $f['value'] = $record[$f['name']];
    }
    return $this->form;
  }

}
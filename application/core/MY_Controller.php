<?php defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

  var $controller, $model, $page_title;

  function __construct () {
    parent::__construct();
    $this->load->helper('url');
    $this->load->library('session');
    $this->controller = $this->router->fetch_class();
    $this->model = $this->controller . 's';
    $this->load->model($this->model);
  }

  public function loadview ($view, $vars = array()) {
    $vars['system_name']  = $this->db->get_where('settings', array('type' => 'system_name'))->row()->description;
    $vars['system_title'] = $this->db->get_where('settings', array('type' => 'system_title'))->row()->description;
    $vars['text_align']   = $this->db->get_where('settings', array('type' => 'text_align'))->row()->description;
    $vars['account_type'] = $this->session->userdata('login_type');
    $vars['page_title']   = strlen($this->page_title) > 0 ? get_phrase($this->page_title): $this->controller;

    $vars['menu'] = array();
    $submenu = array();
    foreach ($this->db->get('menu')->result() as $menu) {
      if (!in_array($vars['account_type'], explode(',', $menu->roles))) continue;
      if (empty ($menu->parent)) $vars['menu'][$menu->uuid] = array('menu' => $menu, 'submenu' => array());
      else $submenu[] = $menu;
    }
    foreach ($submenu as $sub) $vars['menu'][$sub->parent]['submenu'][] = $sub;

    if (file_exists('uploads/' . $vars['account_type'] . '_image/' . $this->session->userdata('login_user_id') . '.jpg')) $vars['photo_profile'] = base_url() . 'uploads/' . $type . '_image/' . $id . '.jpg';
    else $vars['photo_profile'] = base_url() . 'uploads/user.jpg';

    if (!isset ($vars['form_action'])) $vars['form_action'] = site_url($this->controller);
    $vars['current'] = array (
      'model' => $this->model,
      'controller' => $this->controller
    );
    $this->load->view($view, $vars);
  }

  public function index () {
    $model = $this->model;
    if ($post = $this->input->post()) {
      if ($post['last_submit'] !== $this->session->userdata('last_submit')) {
        $this->session->set_userdata('last_submit', $post['last_submit']);
        unset($post['last_submit']);
        if (isset ($post['delete'])) $this->$model->delete($post['delete']);
        else $this->$model->save($post);
      }
    }
    $vars = array();
    $vars['page_name'] = 'table';
    $vars['records'] = $this->$model->find();
    $vars['thead'] = $this->$model->thead;
    $this->loadview('mitraklinik/index', $vars);
  }

  function create () {
    $model= $this->model;
    $vars = array();
    $vars['page_name'] = 'form';
    $vars['form']     = $this->$model->getForm();
    $this->loadview('mitraklinik/index', $vars);
  }

  function read ($id) {
    $data = array();
    $data['page_name'] = 'form';
    $model = $this->model;
    $data['form'] = $this->$model->getForm($id);
    $this->loadview('mitraklinik/index', $data);
  }

  function delete ($uuid) {
    $vars = array();
    $vars['page_name'] = 'confirm';
    $vars['uuid'] = $uuid;
    $this->loadview('mitraklinik/index', $vars);
  }

  function select2 ($model, $field) {
    $this->load->model($model);
    echo '{"results":'. json_encode($this->$model->select2($field, $this->input->post('term'))) . '}';
  }

}
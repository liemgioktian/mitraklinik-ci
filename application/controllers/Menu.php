<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends MY_Controller {

  function dashboard() {
    $page_data['page_name'] = 'dashboard';
    $page_data['page_title']= get_phrase('admin_dashboard');
    $this->loadview('mitraklinik/index', $page_data);
  }

}